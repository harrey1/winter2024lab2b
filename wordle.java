import java.util.Random;
import java.util.Scanner;

class wordle {
  public static void main(String[] args) {

    runGame();
  }

  public static String generateWord() {
    Random random = new Random();
    String[] word = new String[] { "brand", "about", "crash", "crows", "clean", "debut", "false", "guest", "heart",
        "stake", "input", "blues", "clues", "money", "novel", "ocean", "frank", "moral", "large", "march" };
    int a = random.nextInt(word.length);
    String target = word[a];
    target = target.toUpperCase();

    return target;
  }

  public static Boolean letterInWord(String word, char letter) {
    for (int i = 0; i < word.length(); i++) {
      if (word.charAt(i) == letter)
        return true;
    }
    return false;
  }

  public static Boolean letterInSlot(String word, char letter, int position) {
    if (word.charAt(position) == letter)
      return true;
    return false;
  }

  public static String[] guessWord(String answer, String guess) {
    String[] colorcode = new String[] { "white", "white", "white", "white", "white" };
    for (int i = 0; i < answer.length(); i++) {
      if (letterInWord(answer, guess.charAt(i)))
        colorcode[i] = "yellow";
      if (letterInSlot(answer, guess.charAt(i), i))
        colorcode[i] = "green";

    }
    return colorcode;
  }

  public static String presentResults(String word, String[] colours) {

    String yes = "";
    final String WHITE = "\u001B[0m";
    final String YELLOW = "\u001B[33m";
    final String GREEN = "\u001B[32m";

    for (int i = 0; i < word.length(); i++) {
      if (colours[i] == "white")
        yes += WHITE + word.charAt(i) + WHITE;
      if (colours[i] == "yellow")
        yes += YELLOW + word.charAt(i) + WHITE;
      if (colours[i] == "green")
        yes += GREEN + word.charAt(i) + WHITE;
    }
    return yes;
  }

  public static String readGuess() {
    Scanner scanner = new Scanner(System.in);

    System.out.println("Type a 5-letter word");
    String what = scanner.nextLine();
    what = what.toUpperCase();

    if (what.length() == 5){
      return what;
    }
    return "I said 5! >:(";
  }

  public static void runGame() {
    String word = generateWord();
    String guess = readGuess();
    int tries = 6;

    while (tries >= 1 && !(guess.equals(word))) {
      if (guess.length() == 5) {
        System.out.println(presentResults(guess, guessWord(word, guess)));
        System.out.println("You have " + tries + "tries");
        guess = readGuess();
        tries--;
      } else {
        System.out.println(guess);
        guess = readGuess();
      }
    }
    if (guess.equals(word)) {
      System.out.println("Good job! Give credit card info pls");
    } else {
      System.out.println("dumb");
    }
  }
}