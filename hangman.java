import java.util.Scanner;
public class hangman{
	
	public static int isLetterInWord(String word, char c){
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c)
				return i;
			
		}
		return -1;
	}
	
	public static char toUpperCase(char c){
		c = Character.toUpperCase(c);
		return c;
	}
	
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String four = "";
		if(letter0 == true){
			four += word.substring(0,1);
		}else{
		four += '_';
		}
		if(letter1 == true){
			four += word.substring(1,2);
		}else{
		four += '_';
		}
		if(letter2 == true){
			four += word.substring(2,3);
		}else{
		four += '_';
		}
		if(letter3 == true){
			four += word.substring(3,4);
		}else{
		four += '_';
		}
		
		System.out.println(four);
	}
	
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		int count = 0;
		

		
		while(count < 6 && (!letter0 || !letter1 || !letter2 || !letter3)){
			System.out.println("Please enter a letter");
			char c = reader.next().charAt(0);
			
			
			
			int index = isLetterInWord(word, c);
			if(index == 0)
				letter0 = true;
			if(index == 1)
				letter1 = true;
			if(index == 2)
				letter2 = true;
			if(index == 3)
				letter3 = true;
			if(index == -1)
				count++;
			printWork(word, letter0, letter1, letter2, letter3);
			
			System.out.println("You lost " + count + " heart(s)");
		}
		
		if(count < 6)
			System.out.println("Good job!! Now give me your credit card info");
		
		if(count == 6)
			System.out.println("Loser ;( Get better");
	}
}