import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Hello player!");
		System.out.println("Type 1 if you want to play Hangman. Type 2 if you want to play Wordle.");
		
		int x = reader.nextInt();
		reader.nextLine();
		
		//Player has to pick between 1 and 2, if not game closes for not following the rule.
		if(x > 2){
			System.out.println("Please type 1 or 2!");
		}
		if(x == 1){
			hangman();
		}
		if(x == 2){
			wordle();
		}
	}
	
	//if they press 1, it brings them to hangman
	public static void hangman(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Please enter a 4 letter word");
		String word = reader.nextLine();
		hangman.runGame(word);
		
		System.out.println("Would you like to play another game? [1]: YES; [2]: NO");
		
		int x = reader.nextInt();
		reader.nextLine();
		
		//player gets to pick if they want to play another game or continue playing the game. If they press 1, wordle starts playing. If 2, game continues
		if(x > 2){
			System.out.println("Please type 1 or 2!");
		}
		
		if (x == 1){
			wordle();
		}
		
		if (x == 2){
			hangman();
		}
	}
	
	//if they press 2, it brings them to wordle
	public static void wordle(){
		wordle.runGame();
		Scanner reader = new Scanner(System.in);
		
		
		System.out.println("Would you like to play another game? [1]: YES; [2]: NO");
		
		int x = reader.nextInt();
		reader.nextLine();
		
		//player gets to pick if they want to play another game or continue playing the game. If they press 1, hangman starts playing. If 2, game continues
		if(x > 2){
			System.out.println("Please type 1 or 2!");
		}
		
		if (x == 1){
			hangman();
		}
		
		if (x == 2){
			wordle();
		}
	}
}